# 2023 ACM SIGGRAPH Labs

## Sketching Pipelines for Ephemeral Immersive Spaces Labs

This repository contains materials to be used by attendees of Hands-On Class "Sketching Pipelines for Ephemeral Immersive Spaces" at SIGGRAPH 2023.
